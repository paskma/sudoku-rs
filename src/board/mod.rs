
pub struct BoardData {
    data : [u8; 81]
}

struct Possibility {
    row : usize,
    col : usize,
    numbers : Vec<u8>
}

pub trait Board {
    fn new_empty() -> Self;
    fn new_with_array(data : [u8; 81]) -> Self;
    fn get(&self, row : usize, col : usize) -> u8;
    fn set(&mut self, row : usize, col : usize, value : u8);
    fn fits_in_row(&self, n : u8, col : usize) -> bool;
    fn fits_in_col(&self, n : u8, row : usize) -> bool;
    fn fits_in_block(&self, n : u8, row : usize, col : usize) -> bool;
    fn fits(&self, n : u8, row : usize, col : usize) -> bool;
    fn possible(&self, row : usize, col : usize) -> Vec<u8>;
    fn solve_impl(&mut self, level : usize) -> bool;
    fn debug_print(&self);
}

pub trait SliceExt2 {
    type Item;
    fn min_item<F>(&self, less_than: F) -> usize where F: FnMut(&Self::Item, &Self::Item) -> bool;
}

impl<T> SliceExt2 for [T] {
    type Item = T;

    fn min_item<F>(&self, mut less_than: F) -> usize where F: FnMut(&T, &T) -> bool {
        if self.len() == 0 {
            panic!("Does not work for empty slices");
        }
        
        let mut result : usize = 0;
        
        for i in 1..self.len() {
            if less_than(&self[i], &self[result]) {
                result = i;
            }
        }
        
        
        result
    }
}

#[test]
fn test_min_item_one() {
    let v = [1];
    let i = v.min_item(|a, b| a < b);
    assert_eq!(i, 0);
}

#[test]
fn test_min_item_two() {
    let v = [2, 1];
    let i = v.min_item(|a, b| a < b);
    assert_eq!(i, 1);
}

#[test]
fn test_min_item_first() {
    let v = [2, 1, 1];
    let i = v.min_item(|a, b| a < b);
    assert_eq!(i, 1);
}



impl Board for BoardData {
    fn new_empty() -> BoardData {
        return BoardData { data : [0; 81] }
    }
    
    fn new_with_array(data : [u8; 81]) -> BoardData {
        return BoardData { data : data }
    }

    fn get(&self, row : usize, col : usize) -> u8 {
        self.data[row * 9 + col]
    }
    
    fn set(&mut self, row : usize, col : usize, value : u8) {
        self.data[row * 9 + col] = value;
    }
    
    fn fits_in_row(&self, n : u8, row : usize) -> bool {
        for k in 0..9 {
            if self.get(row, k) == n {
                return false
            }
        }
        
        true
    }
    
    fn fits_in_col(&self, n : u8, col : usize) -> bool {
        for k in 0..9 {
            if self.get(k, col) == n {
                return false
            }
        }
        
        true
    }
    
    
    fn fits_in_block(&self, n : u8, row : usize, col : usize) -> bool {
        for k in 0..3 {
            for l in 0..3 {
                if self.get(k + row - row%3, l + col - col%3) == n {
                    return false
                }
            }
        }
        
        true
    }
    
    fn fits(&self, n : u8, row : usize, col : usize) -> bool {
        self.fits_in_row(n, row) 
        && self.fits_in_col(n, col) 
        && self.fits_in_block(n, row, col)
    }
    
    fn possible(&self, row : usize, col : usize) -> Vec<u8> {
        let mut result : Vec<u8> = Vec::new();
        
        if self.get(row, col) != 0 {
            return result;
        }
        
        result.reserve(9);
        
        for i in 1..10 {
            if self.fits(i, row, col) {
                result.push(i);
            }
        }
        
        result
    }
    
    fn solve_impl(&mut self, level : usize) -> bool {
        let mut ps : Vec<Possibility> = Vec::with_capacity(81);
        let mut zeroes : usize = 0;
        
        for row in 0..9 {
            for col in 0..9 {
                if self.get(row, col) == 0 {
                    zeroes += 1;
                } else {
                    continue;
                }
                
                let possible_nums = self.possible(row, col);
                if possible_nums.len() > 0 {
                    ps.push(Possibility{row : row, col: col, numbers : possible_nums});
                } else {
                    return false;
                }
            }
        }
        
        if zeroes == 0 {
            return true
        }
        
        if ps.len() == 0 {
            return false
        }
        
        let current = &ps[ps.min_item(|a, b| a.numbers.len() < b.numbers.len())];
        
        for i in current.numbers.iter() {
            self.set(current.row, current.col, *i);
            
            let solved = self.solve_impl(level + 1);
            if solved {
                return true;
            }
        }
        
        self.set(current.row, current.col, 0);
        return false
    }
    
    fn debug_print(&self) {
        for row in 0..9 {
            for col in 0..9 {
                print!("{}", self.get(row, col));
            }
            println!("");
        }
    }
}


#[test]
fn test_get_and_set() {
    let mut b : BoardData = Board::new_empty();
    assert_eq!(0, b.get(1, 1));
    b.set(1, 1, 9);
    assert_eq!(9, b.get(1, 1));
}


#[test]
fn test_fits_in_row() {
    let mut b : BoardData = Board::new_empty();
    assert_eq!(true, b.fits_in_row(1, 0));
    b.data[8] = 1;
    assert_eq!(false, b.fits_in_row(1, 0));
}



